#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]){
  if (argc != 4){
    printf("Usage: %s <int> <operator> <int>\n", argv[0]);
    return 1;
  }
  int v1 = atoi(argv[1]);
  int v2 = atoi(argv[3]);
  char op = *argv[2];

  switch (op) {
      case '+':
	printf("%i %c %i = %i\n", v1, op, v2, (v1+v2));
	break;
      case '-':
	printf("%i %c %i = %i\n", v1, op, v2, (v1-v2));
	break;
      case '*':
	printf("%i %c %i = %i\n", v1, op, v2, (v1*v2));
	break;
      case '/':
	printf("%i %c %i = %i\n", v1, op, v2, (v1/v2));
	break;
      default:
	printf("%c n'est pas un opérateur supporté.\n", *argv[2]);
	break;
  }
  return 0;
}
