#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void createFile(FILE *file)
{
    fputs("The quick brown Fox jumps over the Fence\n", file);
    fputs("hack the planet!\n", file);
}

void toUpper(FILE *file, FILE *temp)
{
    int c;
    c = fgetc(file);
    while (c != EOF)
    {
        if (islower(c))
        {
            fputc((c - 32),temp);
        }
        else
        {
            fputc(c,temp);
        }
        c = fgetc(file);
    }
}

void showFile(FILE *file)
{
    char *content = (char *)calloc(1024, sizeof(char));
    while (fgets(content, 1024, file))
    {
        printf("%s",content);
    }
}

int main()
{
    FILE *file;
    FILE *temp;
    char name[] = "file.txt";
    char nameT[] = "temp";
    temp = fopen(nameT,"w+");
    file = fopen(name, "w+");

    createFile(file);
    rewind(file);
    toUpper(file, temp);
    rewind(temp);
    showFile(temp);

    fclose(file);
    fclose(temp);
    remove(name);
    rename(nameT, name);
    return 0;
}