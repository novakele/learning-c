#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Employee
{
    char *name;
    struct HireDate
    {
        int day;
        int month;
        int year;
    }hireDate;
    float salary;
};
void clearBuff()
{
    int c = getchar();
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}
int main()
{
    struct Employee employee, *pE;
    pE = &employee;
    pE->name = (char *)calloc(100, sizeof(char));
    strcpy(pE->name,"Bobby Ryan");
    pE->hireDate.day = 19;
    pE->hireDate.month = 11;
    pE->hireDate.year = 2019;
    pE->salary = 60000;

    struct Employee employee1;
    pE = &employee1;
    pE->name = (char *)calloc(100, sizeof(char));
    printf("What is your name? ");
    fgets(pE->name, 100, stdin);
    printf("What day were you hired? ");
    scanf("%d",&pE->hireDate.day);
    printf("What month were you hired? ");
    scanf("%d",&pE->hireDate.month);
    printf("What year were you hired? ");
    scanf("%d",&pE->hireDate.year);
    printf("What is your salary? ");
    scanf("%f",&pE->salary);

    pE = &employee;
    printf("\nName: %s\n", pE->name);
    printf("Hired date: %d/",pE->hireDate.day);
    printf("%d/%d\n",pE->hireDate.month,pE->hireDate.year);
    printf("Salary: %.2f$\n\n",pE->salary);

    pE = &employee1;
    printf("Name: %s", pE->name);
    printf("Hired date: %d/",pE->hireDate.day);
    printf("%d/%d\n",pE->hireDate.month,pE->hireDate.year);
    printf("Salary: %.2f$\n\n",pE->salary);

    return 0;
}