#include <stdio.h>
#include <stdlib.h>

// https://pconrad.github.io/old_pconrad_cs16/topics/cmdlineargs/

int main(int argc, char *argv[]){
  if (argc != 3) {
    printf("Usage: ./challengeRectangle <height> <lenght>\n");
    return 1;
  }
  
  double h = atof(argv[1]);
  double l = atof(argv[2]);
  double perimetre = 2.0 * (h+l);
  double aire = h * l;
  
  printf("Le périmètre est de %.2f.\nL'aire est de %.2f.\n", perimetre, aire);

  return 0;
}
