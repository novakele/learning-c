#include <stdio.h>

void swapValue(int x, int y)
{
    int temp = x;
    x = y;
    y = temp;
}

void swapReference(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

int main()
{
    int x = 5;
    int y = 10;

    printf("Before swapValue:\nx = %d\ny = %d\n\n",x,y);
    swapValue(x,y);
    printf("After swapValue:\nx = %d\ny = %d\n\n",x,y);

    printf("Before swapReference:\nx = %d\ny = %d\n\n",x,y);
    swapReference(&x,&y);
    printf("After swapReference:\nx = %d\ny = %d\n\n",x,y);

    return 0;
}