#include <stdio.h>
#include <stdlib.h>

void createFile(FILE *file)
{
    int c;
    for (c = 0; c < 130; c++)
    {
        if (c % 2)
        {
            fputc(c,file);
            fputc('\n',file);
        }
    }
}

int countLines(FILE *file)
{
    int i = 0;
    int c;
    c = fgetc(file);
    while (c != EOF)
    {
        if (c == '\n')
        {
            i++;
        }
        c = fgetc(file);
    }
    return i;
}

int main()
{
    FILE *file;
    char *name = (char *)calloc(25, sizeof(char));
    name = "file.txt";
    file = fopen(name,"w");
    createFile(file);
    fclose(file);
    file = fopen(name, "r");
    printf("There are %d lines in %s\n",countLines(file), name);
    fclose(file);
    return 0;
}