#include <stdio.h>
#include <stdbool.h>

int main() {
  int prime[100] = {2,3};
  int i;
  int j;
  bool check;
  for (i = 5; i < 100; i+=2){
    check = true;
    if (i % 2 == 1) {
      for (j = 0; j < 100; j++) {
	if (prime[j] != 0) {
	  if (i % prime[j] == 0) {
	    check = false;
	  }
	}
      }
    }
    if (check) {
      prime[i] = i;
    }
  }
  for (i = 0; i < 100; i++) {
    if (prime[i] != 0) { 
      printf("%i\n",prime[i]);
    }
  }
  return 0;
}
