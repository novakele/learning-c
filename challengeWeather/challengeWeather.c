#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  time_t t;
  srand((unsigned)time(&t));
  int i;
  int j;
  float stats[5][12] = {
		      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
  };
  float yearStats[5];
  float monthStats[12];
  int nbrTableau = sizeof(stats);
  int nbrEntrees = sizeof(stats[0]);
  int sizeEntree = sizeof(stats[0][0]);

  for (i = 0; i < nbrTableau/nbrEntrees; i++) {
    for (j = 0; j < nbrEntrees/sizeEntree; j++) {
      stats[i][j] = (float)rand()/RAND_MAX*rand()/10000000;
    }
  }

  for (i = 0; i < nbrTableau/nbrEntrees; i++) {
    for (j = 0; j < nbrEntrees/sizeEntree; j++) {
      yearStats[i] = yearStats[i] + stats[i][j];
    }
  }

  for (j = 0; j < nbrEntrees/sizeEntree; j++) {
    for (i = 0; i < nbrTableau/nbrEntrees; i++) {
      monthStats[j] = monthStats[j] + stats[i][j];
    }
    monthStats[j] = monthStats[j] / 12;
  }

  for (i = 0; i < sizeof(yearStats)/sizeof(yearStats[0]); i++) {
    printf("%.2fmm de pluie en 201%i\n",yearStats[i], i);
  }
  printf("\nMoyenne de pluie par mois.\n");
  printf(" JAN   FEV   MAR   APR   MAI   JUN   JUL   AUG   SEP   OCT   NOV   DEC \n");
  for (j = 0; j < sizeof(monthStats)/sizeof(monthStats[0]); j++) {
    printf("%.2f ",monthStats[j]);
  }
  printf("\n");
  return 0;

  
}
