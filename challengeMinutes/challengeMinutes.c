#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
  if (argc != 2) {
    printf("Usage: %s <minutes>\n",argv[0]);
    return 1;
  }
  double toConvert = atof(argv[1]);
  double minutesInDay = 60*24;
  double minutesInYear = minutesInDay*365;
  double convertDay = toConvert / minutesInDay;
  double convertYear = toConvert / minutesInYear;

  printf("\n%.0f minutes is %.2f days and %.3f years.\n",
	 toConvert, convertDay, convertYear);
  return 0;
}
