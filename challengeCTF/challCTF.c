#include <stdio.h>
#include <curl/curl.h>
#include <stdbool.h>
#include <unistd.h>
// https://ec.haxx.se/libcurl--libcurl.html

int main() {
  int i = 0;
  bool valide = false;
  char rep[100];
  int anArray[ 27 ] = {104, 116, 116, 112, 58, 47, 47, 103, 111, 102, 105, 108, 101, 46, 105, 111, 47, 63, 99, 61, 50, 119, 114, 97, 86, 54, 0};
  int j = sizeof(anArray) / sizeof(anArray[0]);
  char k[27] = {};

  for (i=0; i<j; i++) {
    k[i] += anArray[i];
  }
  printf("[*] Connexion à la page Facebook de Yasser...\n");
  sleep(3);
  printf("[*] Connexion établie.\n");
  sleep(2);
  printf("[*] Recherche de son plat préféré...\n");
  sleep(2);
  printf("[*] Plat préféré trouvé!\n");
  sleep(2);

  while (! valide) {
    CURLcode ret;
    CURL *curl;

    curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 102400L);
      curl_easy_setopt(curl, CURLOPT_URL, k);
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
      curl_easy_setopt(curl, CURLOPT_USERAGENT,"curl/7.6.0");
      curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
      curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
      curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);

      ret = curl_easy_perform(curl);
      curl_easy_cleanup(curl);
      curl = NULL;
    }
    printf("[?] Quel est son plat préféré? ");
    fgets(rep, 100, stdin);
    sleep(2);
    printf("[*] Vérification de la répone...\n");
    sleep(4);
    printf("[!] Réponse invalide.\n\n\n");
  }
  
  return 0;
}
