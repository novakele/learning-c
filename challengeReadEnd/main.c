#include <stdio.h>
#include <stdlib.h>

void fromEnd(FILE *file)
{
    fseek(file, 0, SEEK_END);
    int size, i;
    size = (int)ftell(file);
    fseek(file,0,SEEK_SET);
    char *content = (char *)calloc(size, sizeof(char));
    for (i = (size - 1); i >= 0; i--)
    {
        content[i] = (char)fgetc(file);
    }
    content[size] = 0;
    printf("%s",content);
}
int main()
{
    char name[] = "file.txt";
    FILE *file = fopen(name, "r");
    fromEnd(file);
    fclose(file);
    return 0;
}