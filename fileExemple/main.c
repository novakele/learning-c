#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp;
    int c;
    char *str = (char *)calloc(1024, sizeof(char));

    fp = fopen("foo.txt","r");

    if (fp == NULL)
    {
        perror("Error in opening file\n");
        return -1;
    }
    if (fgets(str, 1024, fp) != NULL)
    {
        printf("%s", str);
    }
    fclose(fp);
    return 0;
}