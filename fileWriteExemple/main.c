#include <stdio.h>
#include <stdlib.h>

void readString(FILE *fileP)
{
    char *ligne = (char *)calloc(1024, sizeof(char));
    fgets(ligne, 1024, fileP);
    printf("%s\n",ligne);
    free(ligne);
    fclose(fileP);
}

void writeChar(FILE *fileP)
{
    int ch;
    for (ch = 33; ch <= 122; ch++)
    {
        fputc(ch, fileP);
    }
    fputc('\n',fileP);
    fputc(EOF,fileP);
    fclose(fileP);
}

int main()
{
    FILE *fp;
    fp = fopen("write.txt","w+");
    writeChar(fp);
    fp = fopen("write.txt","r");
    readString(fp);
    return 0;
}