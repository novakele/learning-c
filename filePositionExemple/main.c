#include <stdio.h>
#include <stdlib.h>
int main()
{
    FILE *fp;
    int len;
    fp = fopen("file.txt","r");
    if (fp == NULL)
    {
        perror("Error!");
        return -1;
    }
    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    fclose(fp);

    printf("Total size of file.txt = %d\n", len);
    fclose(fp);
    return 0;
}