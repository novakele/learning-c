#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "challengeStringFunctions.h"
#define MAX 100

void bubbleSort(int nbrElements, char param[][MAX])
{
  int i;
  int j;
  char temp[MAX];

  for (j = 0; j < (nbrElements-1); j++)
    {
      for (i = (j+1); i < nbrElements; i++)
	{
	  if (strcmp(param[j], param[i]) > 0)
	    {
	      strcpy(temp, param[j]);
	      strcpy(param[j], param[i]);
	      strcpy(param[i], temp);
	    }
	}
    }
}

void reverseString(const char *s)
{
  int sLength = strlen(s);
  char reversedString[sLength];
  int i;
  int j = 0;
  for (i = sLength - 1; i >= 0; i--)
    {
      reversedString[j] = s[i];
      j++;
    }
  reversedString[j] = '\0';
  printf("%s\n",reversedString);
}

int main()
{
  int nbrStrings;
  int i;
  char str[100];
  printf("Input string: ");
  fgets(str,100,stdin);
  reverseString(str);

  printf("Input number of strings: ");
  scanf("%d",&nbrStrings);
  char needSorting[nbrStrings][MAX];
  for (i = 0; i < nbrStrings; i++)
    {
      printf("String %d: ",i+1);
      scanf("%s",needSorting[i]);
    }
  bubbleSort(nbrStrings, needSorting);

  for (i = 0; i < nbrStrings; i++)
    {
      printf("%d. %s\n",i+1,needSorting[i]);
    }
  return 0;  
}
