#include <stdio.h>

int main()
{
    int number = 15;
    int *pnumber = &number;

    printf("Adresse of number: %p\n", &number);
    printf("Address of pnumber: %p\n", &pnumber);
    printf("Value of pnumber: %p\n", pnumber);
    printf("Value of pnumber pointing to: %d\n", (int)*pnumber);

    return 0;
}