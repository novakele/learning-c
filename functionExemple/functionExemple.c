#include <stdio.h>
#include "functionExemple.h"

void printMessage(char msg[]) {
  printf("%s\n",msg);
}

int addition(int x, int y) {
  return x+y;
}

int main(int argc, char *argv[]) {
  char *msg = "Hello, World!";
  int z;
  printMessage(msg);
  z = addition(10,5);
  printf("La somme de 10 et 5 est %d\n",z);
  return 0;
}




