#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s <nombre d'heures travaillés>\n",argv[0]);
    return 1;
  }
  float payRate = 12.00; 
  float nbrHeures = atof(argv[1]);
  float salaireBrute;
  float taxes;
  float salaireNet;
  
  if (nbrHeures > 40) {
    salaireBrute = ((nbrHeures - 40) * (payRate * 1.5)) + (40 * payRate);
  } else {
    salaireBrute = (nbrHeures * payRate);
  }

  if (salaireBrute > 450) {
    taxes = ((salaireBrute - 450) * 0.25) + (300 * 0.15) + (150 * 0.20);
  }
  else if (salaireBrute > 300) {
    taxes = ((salaireBrute - 300) * 0.20) + (300 * 0.15);
  }
  else {
    taxes = (salaireBrute * 0.15);
  }

  salaireNet = salaireBrute - taxes;
  printf("%.2f$ - %.2f$ = %.2f$\n", salaireBrute, taxes, salaireNet);

  return 0;
}
