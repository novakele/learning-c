#include <stdio.h>

int main(int argc, char *argv[]){
  enum Company {GOOGLE, FACEBOOK, XEROX, YAHOO, EBAY, MICROSOFT};
  enum Company first;
  enum Company second;
  enum Company third;

  first = XEROX;
  second = GOOGLE;
  third = EBAY;

  printf("%i\n%i\n%i\n", first, second, third);

  return 0;
}
