#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
  if (argc != 2){
    printf("Usage: %s <integer>\n", argv[0]);
    return 1;
  }

  atoi(argv[1]) % 2 ? printf("odd\n") : printf("even\n");
  return 0;
}
