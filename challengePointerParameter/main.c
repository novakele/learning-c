#include <stdio.h>
#include <stdlib.h>

void *square(int * const x)
{
    (*x) *= (*x);
    return x;
}

int main()
{
    int x = 5;
    int *y = (int *)malloc(sizeof(int));
    *y = 10;

    printf("%d squared is %d\n",x,*(int *)square(&x));
    printf("%d squared is %d\n",*y,*(int *)square(y));
    return 0;
}