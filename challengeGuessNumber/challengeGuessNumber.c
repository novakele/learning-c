#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

int main() {
  time_t t;
  srand((unsigned) time(&t));
  int randint = rand() % 21;
  bool found = false;
  char guess[3];
  int  nbrEssais = 0;
  int num;
  int c;
  while (! found && nbrEssais < 5) {

    printf("Il vous reste %i essais.\n",(5 - nbrEssais));
    
    do {
      printf("Entrez un nombre entre 0 et 20: \n");
      fgets(guess, 4, stdin);
      //https://stackoverflow.com/questions/47320496/how-to-determine-if-stdin-is-empty
      if ((fseek(stdin, 0, SEEK_END), ftell(stdin)) > 0) {
	while ((c = getchar()) != '\n' && c != EOF);
      }
      num = atoi(guess);
    } while (num < 0 || num > 20);
      
    if (num == randint) {
      found = true;
      printf("Vous avez gagné!\n");
    } else if (num > randint) {
      printf("Le nombre est plus petit que %s\n", guess);
      nbrEssais++;
    } else if (num < randint) {
      printf("Le nombre est plus grand que %s\n", guess);
      nbrEssais++;
    }
  }
  if (! found) {
    printf("Le nombre que vous cherchiez était %i.\n", randint);
  }
  return 0;
}
