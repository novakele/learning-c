#include <stdio.h>

int main(){
   printf("\nByte size of:\nint: %lu\nchar: %lu\nlong: %lu\n long long: %lu\ndouble: %lu\nlong double: %lu",
	sizeof(int), sizeof(char), sizeof(long), sizeof(long long), sizeof(double), sizeof(long double));
  return 0;
}
