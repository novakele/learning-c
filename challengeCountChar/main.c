#include <stdio.h>

int strLen(const char *string)
{
    const char *start = string;
    while (*string)
    {
        *string++;
    }
    return (int)(string - start);
}

int main()
{
    char string[] = "The quick brown fox jumps";
    printf("%s is of length %d\n", string, strLen(string));
    return 0;
}