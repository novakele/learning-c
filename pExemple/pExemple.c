#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main() {
  char str[] = "The quick brown fox";
  char q = 'q';
  char* pGotChar = NULL;
  pGotChar = strchr(str,q);

  printf("Adresse où la lettre %c se trouve: %p\n", q, pGotChar);
  printf("Contenu de l'adresse %p: %c\n", pGotChar, *pGotChar);
  return 0;
}
