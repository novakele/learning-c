#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "challengeTicTacToe.h"

bool checkWin(char game[3][3]){
  if (game[0][0] == game[0][1] && game[0][0] == game[0][2]) {
    return true;
  } else if (game[1][0] == game[1][1] && game[1][0] == game[1][2]) {
    return true;
  } else if (game[2][0] == game[2][1] && game[2][0] == game[2][2]) {
    return true;
  } else if (game[0][0] == game[1][0] && game[0][0] == game[2][0]) {
    return true;
  } else if (game[0][1] == game[1][1] && game[0][1] == game[2][1]) {
    return true;
  } else if (game[0][2] == game[1][2] && game[0][2] == game[2][2]) {
    return true;
  } else if (game[0][0] == game[1][1] && game[0][0] == game[2][2]) {
    return true;
  } else if (game[0][2] == game[1][1] && game[0][0] == game[2][0]) {
    return true;
  } else {
    return false;
  }
}

void drawBoard(char game[3][3]){
  system("clear");
  printf("        Tic Tac Toe      \n\n");
  printf("Payer 1 (X)  -  Player 2 (O)\n\n");
  printf("          |     |      \n");
  printf("       %c  |  %c  |  %c  \n",game[0][0],game[0][1],game[0][2]);
  printf("     _____|_____|_____\n");
  printf("          |     |      \n");
  printf("       %c  |  %c  |  %c  \n",game[1][0],game[1][1],game[1][2]);
  printf("     _____|_____|_____\n");
  printf("          |     |      \n");
  printf("       %c  |  %c  |  %c  \n",game[2][0],game[2][1],game[2][2]);
  printf("          |     |      \n");
}

bool markBoard(char game[3][3], char move, char player) {
  bool valide = false;
  switch(move) {
  case '1':
    if (game[0][0] == '1') {
      game[0][0] = player;
      valide = true;
    }
    break;
  case '2':
    if (game[0][1] == '2') {
      game[0][1] = player;
      valide = true;
    }
    break;
  case '3':
    if (game[0][2] == '3') {
      game[0][2] = player;
      valide = true;
    }
    break;
  case '4':
    if (game[1][0] == '4') {
      game[1][0] = player;
      valide = true;
    }
    break;
  case '5':
    if (game[1][1] == '5') {
      game[1][1] = player;
      valide = true;
    }
    break;
  case '6':
    if (game[1][2] == '6') {
      game[1][2] = player;
      valide = true;
    }
    break;
  case '7':
    if (game[2][0] == '7') {
      game[2][0] = player;
      valide = true;
    }
    break;
  case '8':
    if (game[2][1] == '8') {
      game[2][1] = player;
      valide = true;
    }
    break;
  case '9':
    if (game[2][2] == '9') {
      game[2][2] = player;
      valide = true;
    }
    break;
  default:
    printf("Invalid move.\n");
  }
  return valide;
}

void emptyBuffer() {
  int c;
  if ((fseek(stdin, 0, SEEK_END), ftell(stdin)) > 0) {
    while ((c = getchar()) != '\n' && c != EOF);
  }
}

int main() {
  char game[3][3] = {{'1','2','3'},
		     {'4','5','6'},
		     {'7','8','9'}};
  int turn = 0;
  char move = '\0';
  char player = '\0'; 
  while (! checkWin(game) && turn < 9) {
    drawBoard(game);
    if (turn % 2 == 0) {
      player = 'X';
    } else {
      player = 'O';
    }
    do {
     printf("Player %i, enter a number: ",(turn % 2 + 1));
     move = getchar();
     emptyBuffer();
    } while (! markBoard(game, move, player));
    ++turn;
  }
  if (checkWin(game)) {
    printf("Congrats, Player %i you won!\n",((turn - 1) % 2 + 1));
  } else if (! checkWin(game) && turn == 9) {
    printf("Null game, better luck next time!\n");
  }
  return 0;
}
