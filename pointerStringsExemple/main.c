#include <stdio.h>

void copyString( char *to, char *from)
{
    while (*from)
    {
        *to++ = *from++;
    }
    *to = '\0';
}

int countString(char *toCount)
{
    int i = 0;
    while (*toCount)
    {
        *toCount++;
        i++;
    }
    return i;
}

int main()
{
    char str1[] = "A string to be copied";
    char str2[250];

    copyString(str2, str1);
    printf("%s\n",str2);
    printf("%d\n",countString(str1));
    return 0;
}