#include <stdio.h>
#include <stdlib.h>

void clearBuffer()
{
    int c = getchar();
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

int main()
{
    int *size = (int *)malloc(sizeof(int));
    printf("Enter the string limit: ");
    scanf("%d", size);
    clearBuffer();

    char *string = (char *) calloc(*size, sizeof(char));
    if (string != NULL && *size > 0)
    {
        printf("Enter the string: ");
        fgets(string, *size, stdin);
        printf("Your string: %s", string);
        free(string);
        return 0;
    }
    return 1;
}