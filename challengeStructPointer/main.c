#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Item
{
    char *name;
    int quantity;
    float price;
    float amount;
};

void readItem(struct Item *item)
{
    printf("Name: ");
    fgets(item->name, 100, stdin);
    printf("Quantity: ");
    scanf("%d",&item->quantity);
    printf("Price: ");
    scanf("%f",&item->price);
    item->amount = (item->price) * (item->quantity);
}

void printItem(struct Item *item)
{
    printf("\nName: %s", item->name);
    printf("Quantity: %d\n", item->quantity);
    printf("Price: %.2f$\n", item->price);
    printf("Amount: %.2f\n", item->amount);
}

int main()
{
    struct Item item, *pI;
    pI = &item;
    pI->name = (char *)calloc(100, sizeof(char));
    readItem(pI);
    printItem(pI);
    return 0;
}