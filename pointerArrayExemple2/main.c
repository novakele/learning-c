#include <stdio.h>
#include <string.h>

int main()
{
    char multiple[] = "a string";
    char *p = multiple;
    int i;
    int strEnd = strnlen(multiple, sizeof(multiple));

    for (i = 0; i < strEnd; i++)
    {
        printf("multiple[%d] = %c\n",i,multiple[i]);
        printf("*(p+%d) = %c\n",i,*(p+i));
        printf("&multiple[%d} = %p\n",i,&multiple[i]);
        printf("p+%d = %p\n\n",i,p+i);
    }
    return 0;
}