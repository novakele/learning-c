#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "challengeCharArray.h"

int stringLength(const char *s)
{
  int i = 0;
  while (s[i] != '\0')
    {
      i++;
    }
  return i;
}

char *stringConcatenate(char *result, const char *str1, const char *str2)
{
  int i;
  int str1L = stringLength(str1);
  int str2L = stringLength(str2);

  if (str1 == NULL || str2 == NULL)
    {
      return NULL;
    }
  
  for (i = 0; i < str1L; i++)
    {
      result[i] = str1[i];
    }
  for (i = 0; i < str2L; i++)
    {
      result[str1L + i] = str2[i];
    }
  return result;
}

bool stringCompare(const char *str1, const char *str2)
{
  int i;
  int str1L = stringLength(str1);
  int str2L = stringLength(str2);
  if (str1L == str2L)
    {
      for (i = 0; i < str1L; i++)
	{
	  if (str1[i] != str2[i])
	    {
	      return false;
	    }
	}
    }
  else
    {
      return false;
    }
  return true;
}

int main()
{
  char result[100];
  const char str1[] = "Hello, ";
  const char str2[] = "World!";
  const char str3[] = "Hello, ";
  const char s[] = "Very long string!";
  printf("Length of \"%s\" is %d\n", s, stringLength(s));
  printf("Concatenated: %s\n",stringConcatenate(result, str1, str2));
  printf("Comparaison: %d\n",stringCompare(str1,str3));
  return 0;
}
