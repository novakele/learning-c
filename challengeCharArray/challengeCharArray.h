int stringLength(const char *s);
char *stringConcatenate(char *result, const char *str1, const char *str2);
bool stringCompare(const char *str1, const char *str2);
