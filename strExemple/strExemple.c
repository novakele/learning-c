#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  char src[50];
  char dst[50];
  int i;
  int j;
  strncpy(src, "This is source!", sizeof(src));
  strncpy(dst, "This is destination!", sizeof(dst));
  i = strlen(src);
  j = strlen(dst);

  printf("Source: %s\nDestination: %s\nLength of source: %i\n\
Length of destination: %d\nConcatenated: %s\n", src, dst, i, j, strncat(src, dst, 50));
  
  return 0;
}
