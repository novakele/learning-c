#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct names
{
    char *first;
    char *last;
};

int main()
{
    struct names aNames, *pNames;
    pNames = &aNames;
    pNames->first = (char *)calloc(50, sizeof(char));
    pNames->last = (char *)calloc(50, sizeof(char));
    strcpy(pNames->first,"Radar");
    strcpy(pNames->last,"La bête du Gévaudan");
    printf("%s %s\n",pNames->first, pNames->last);
    free(pNames->first);
    free(pNames->last);
    return 0;
}