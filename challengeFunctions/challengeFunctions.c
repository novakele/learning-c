#include <stdio.h>
#include <stdlib.h>
#include "challengeFunctions.h"

int gcd(int x, int y) {
  while (x != y) {
    if (x > y) {
      x = x - y;
    } else {
      y = y - x;
    }
  }
  return x;
}

float absolute(float x) {
  if (x > 0) {
    return x;
  } else {
    return x * -1;
  }
}

float squareRoot(float x) {
  if (! (x == absolute(x))) {
      printf("Veuillez entrer un nombre positif.\n");
      return -1.0;
  } else {
    if (x == 0 || x == 1) {
      return x;
    } else {
      int i = 1;
      int result = 1;
      while (result <= x) {
	i++;
	result = i * i;
      }
      return i - 1;
    }
  }
}

int main(int argc, char * argv[]) {
  printf("gcd: %d\n",gcd(atoi(argv[1]),atoi(argv[2])));
  printf("valeur absolue: %.2f\n", absolute(atof(argv[3])));
  printf("racine carrée: %.2f\n", squareRoot(atof(argv[4])));
  return 0;
}
