#include <stdio.h>

struct Time
{
    struct Date
    {
        struct Inception
        {
            int dude;
        } inception;
        int day;
        int month;
        int year;
    } date;
    int hour;
    int minutes;
    int seconds;
};

int main()
{
    struct Time time, *pTime;
    pTime = &time;
    pTime->date.day = 19;
    pTime->date.month = 11;
    pTime->date.year = 2019;
    pTime->hour = 23;
    pTime->minutes = 01;
    pTime->seconds = 58;
    pTime->date.inception.dude = 1;
    printf("We are the %dth day ",pTime->date.day);
    printf("of the %dth month ",pTime->date.month);
    printf("in the year %d\n",pTime->date.year);
    printf("It is %d:0%d:%d\n",pTime->hour,pTime->minutes,pTime->seconds);
    printf("Inception: %d\n",pTime->date.inception.dude);
    return 0;
}