#include <stdio.h>
#include <stdbool.h>
int main() {
  int integerVar = 100;
  float floatVar = 331.45;
  double doubleVar = 8.44e+11;
  bool  boolVar = false;
  char charVar = 'A';
  char buffVar[100] = "Ceci est un message.";

  printf("interger: %d\n", integerVar);
  printf("float: %f\n", floatVar);
  printf("double: %f\n", doubleVar);
  printf("bool: %i\n", boolVar);
  printf("char: %c\n", charVar);
  printf("buff: %s\n", buffVar);
  
  return 0;
}
